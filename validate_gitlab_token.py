#!/usr/bin/env python3

"""
Validate a single or a list of GitLab API tokens.

Brought to you by the Red Team @ GitLab.
"""

import argparse
import requests

def parse_arguments():
    """
    Handles user-passed parameters
    """
    desc = "Check GitLab API key/s for validity"
    parser = argparse.ArgumentParser(description=desc)

    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-k', '--key', type=str, action='append',
                       help='Individual API key')
    group.add_argument('-i', '--infile', type=str, action='store',
                       help='Input file with a single API key per line.')

    parser.add_argument('-t', '--target', type=str, action='append',
                        help='GitLab host. Defaults to gitlab.com. Can be used multiple times')

    args = parser.parse_args()

    if args.infile:
        with open(args.infile) as infile:
            args.key = [key.strip() for key in infile]

    if not args.target:
        args.target = ['gitlab.com']

    return args

def check_key(host, key):
    """
    Validates if an API key is legit
    """
    url = f'https://{host}/api/v4/user'
    headers = {'PRIVATE-TOKEN': key}

    response = requests.get(url, headers=headers)

    if response.status_code == 200:
        user = response.json()['username']
        print(f"[:D] VALID KEY: {key}, USER: {user}")
    elif "Token is expired" in response.text:
        print(f"[:|] EXPIRED KEY: {key}")
    elif response.status_code == 401:
        pass
    else:
        print(f"[!] Unknown reponse for key {key}")
        print(f"    {response.status_code}: {response.reason}")


def main():
    """
    Main program function
    """
    args = parse_arguments()
    for host in args.target:
        print(f"[*] Checking {len(args.key)} API key/s against {host}")
        for key in args.key:
            check_key(host, key)

if __name__ == '__main__':
    main()
